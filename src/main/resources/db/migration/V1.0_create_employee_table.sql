DROP TABLE IF EXISTS EMPLOYEE;
DROP TABLE IF EXISTS COMPANY;
CREATE TABLE EMPLOYEE (
    ID BIGINT NOT NULL AUTO_INCREMENT,
    NAME VARCHAR(255),
    AGE INTEGER,
    GENDER VARCHAR(255),
    SALARY FLOAT(53),
    COMPANY_ID BIGINT,
    PRIMARY KEY (ID)
);
CREATE TABLE COMPANY (
    ID BIGINT NOT NULL AUTO_INCREMENT,
    NAME VARCHAR(255),
    PRIMARY KEY (ID)
);