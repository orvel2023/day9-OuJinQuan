package com.afs.restapi.service.dto;

public class EmployeeRequest {

    private String name;
    private Integer age;
    private String gender;
    private Integer salary;

    public void setCompanyID(Long companyID) {
        this.companyID = companyID;
    }

    private Long companyID;
    public EmployeeRequest(){}

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public Integer getSalary() {
        return salary;
    }

    public Long getCompanyID() {
        return companyID;
    }

    public EmployeeRequest(String name, Integer age, String gender, Integer salary) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
    }

    public EmployeeRequest(String name, Integer age, String gender, Integer salary, Long companyID) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
        this.companyID = companyID;
    }
}
