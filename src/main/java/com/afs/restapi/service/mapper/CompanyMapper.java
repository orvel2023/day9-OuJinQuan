package com.afs.restapi.service.mapper;

import com.afs.restapi.entity.Company;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import org.springframework.beans.BeanUtils;

public class CompanyMapper {
    private CompanyMapper(){

    }
    public  static Company toEntity(CompanyRequest request){
        Company company = new Company();
        BeanUtils.copyProperties(request, company);
        return  company;
    }
    public static CompanyResponse toResponse(Company company){
        CompanyResponse companyResponse = new CompanyResponse();
        BeanUtils.copyProperties(company,companyResponse);
        if(company.getEmployees() != null){
            companyResponse.setEmployeeCount(company.getEmployees().size());
        } else {
            companyResponse.setEmployeeCount(0);
        }
        return companyResponse;
    }
}
