package com.afs.restapi.controller;

import com.afs.restapi.entity.Company;
import com.afs.restapi.service.CompanyService;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("companies")
public class CompanyController {

    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping
    public List<CompanyResponse> getAllCompanies() {
        return companyService.findAll().stream().map(CompanyMapper::toResponse).collect(Collectors.toList());
    }

    @GetMapping(params = {"page", "size"})
    public List<CompanyResponse> getCompaniesByPage(@RequestParam Integer page, @RequestParam Integer size) {
        return companyService.findByPage(page, size).stream().map(CompanyMapper::toResponse).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public CompanyResponse getCompanyById(@PathVariable Long id) {
        return CompanyMapper.toResponse(companyService.findById(id));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public CompanyResponse updateCompany(@PathVariable Long id, @RequestBody CompanyRequest company) {
       return CompanyMapper.toResponse(companyService.update(id, CompanyMapper.toEntity(company)));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long id) {
        companyService.delete(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CompanyResponse createCompany(@RequestBody CompanyRequest company) {
        Company entity = companyService.create(CompanyMapper.toEntity(company)) ;
        return CompanyMapper.toResponse(entity);
    }

    @GetMapping("/{id}/employees")
    public List<EmployeeResponse> getEmployeesByCompanyId(@PathVariable Long id) {
        return companyService.findEmployeesByCompanyId(id).stream().map(EmployeeMapper::toResponse).collect(Collectors.toList());
    }

}
