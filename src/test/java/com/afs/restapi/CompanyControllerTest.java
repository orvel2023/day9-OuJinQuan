package com.afs.restapi;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.CompanyJPARepository;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private CompanyJPARepository companyJPARepository;

    @Autowired
    private EmployeeJPARepository employeeJPARepository;

    @BeforeEach
    void setUp() {
        companyJPARepository.deleteAll();
        employeeJPARepository.deleteAll();
    }

    @Test
    void should_update_company_name() throws Exception {
        CompanyRequest previousCompany = getRequestCompany1();
        Company savedCompany = companyJPARepository.save(CompanyMapper.toEntity(previousCompany));

        CompanyRequest companyUpdateRequest = new CompanyRequest("xyz");
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(companyUpdateRequest);
        mockMvc.perform(put("/companies/{id}", savedCompany.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(204))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(companyUpdateRequest.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.employeeCount").value(0));

    }

    @Test
    void should_delete_company_name() throws Exception {
        CompanyRequest company = getRequestCompany1();
        Company savedCompany = companyJPARepository.save(CompanyMapper.toEntity(company));

        mockMvc.perform(delete("/companies/{id}", savedCompany.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(companyJPARepository.findById(savedCompany.getId()).isEmpty());
    }

    @Test
    void should_create_company() throws Exception {
        CompanyRequest company = getRequestCompany1();

        ObjectMapper objectMapper = new ObjectMapper();
        String companyRequest = objectMapper.writeValueAsString(company);
        mockMvc.perform(post("/companies")
                .contentType(MediaType.APPLICATION_JSON)
                .content(companyRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(result -> assertEquals(companyJPARepository.count(),1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(company.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.employeeCount").value(0));
    }

    @Test
    void should_find_companies() throws Exception {
        CompanyRequest company = getRequestCompany1();
        Company savedCompany = companyJPARepository.save(CompanyMapper.toEntity(company));

        mockMvc.perform(get("/companies"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedCompany.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(company.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].employeeCount").value(0));
    }

    @Test
    void should_find_companies_by_page() throws Exception {
        CompanyRequest company1 = getRequestCompany1();
        CompanyRequest company2 = getRequestCompany2();
        CompanyRequest company3 = getRequestCompany3();
        Company savedCompany1 = companyJPARepository.save(CompanyMapper.toEntity(company1));
        Company savedCompany2 =  companyJPARepository.save(CompanyMapper.toEntity(company2));
        companyJPARepository.save(CompanyMapper.toEntity(company3));

        mockMvc.perform(get("/companies")
                        .param("page", "1")
                        .param("size", "2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedCompany1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(company1.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].employeeCount").value(0))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(savedCompany2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(company2.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].employeeCount").value(0));
    }

    @Test
    void should_find_company_by_id() throws Exception {
        CompanyRequest company = getRequestCompany1();
        Company savedCompany = companyJPARepository.save(CompanyMapper.toEntity(company));
        Employee employee = getEmployee(savedCompany);
        Employee savedEmployee = employeeJPARepository.save(employee);

        mockMvc.perform(get("/companies/{id}", savedCompany.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(savedCompany.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(company.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.employeeCount").value(1));
    }

    @Test
    void should_delete_employee_when_delete_company_given_company() throws Exception {
        CompanyRequest company = getRequestCompany1();
        Company savedCompany = companyJPARepository.save(CompanyMapper.toEntity(company));
        Employee employee = getEmployee(savedCompany);
        Employee savedEmployee = employeeJPARepository.save(employee);

        mockMvc.perform(delete("/companies/{id}", savedCompany.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204))
                .andExpect(result -> assertFalse(companyJPARepository.existsById(savedCompany.getId())))
                .andExpect(result -> assertFalse(employeeJPARepository.existsById(savedEmployee.getId())));
    }

    @Test
    void should_find_employees_by_companies() throws Exception {
        CompanyRequest company = getRequestCompany1();
        Company savedCompany = companyJPARepository.save(CompanyMapper.toEntity(company));
        Employee employee = getEmployee(savedCompany);
        Employee savedEmployee = employeeJPARepository.save(employee);

        mockMvc.perform(get("/companies/{companyId}/employees", savedCompany.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedEmployee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employee.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist());
    }

    private static Employee getEmployee(Company company) {
        Employee employee = new Employee();
        employee.setName("zhangsan");
        employee.setAge(22);
        employee.setGender("Male");
        employee.setSalary(10000);
        employee.setCompanyId(company.getId());
        return employee;
    }
    private static CompanyRequest getRequestCompany1() {
        return new CompanyRequest("ABC");
    }
    private static CompanyRequest getRequestCompany2() {
        return new CompanyRequest("DEF");
    }
    private static CompanyRequest getRequestCompany3() {
        return new CompanyRequest("XYZ");
    }


}