package com.afs.restapi;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.mapper.EmployeeMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private EmployeeJPARepository employeeJPARepository;
    @BeforeEach
    void setUp() {
        employeeJPARepository.deleteAll();
    }

    @Test
    void should_update_employee_age_and_salary() throws Exception {
        EmployeeRequest previousEmployee = new EmployeeRequest( "lisi", 22, "Female", 1000);
        Employee saveEmployee = employeeJPARepository.save(EmployeeMapper.toEntity(previousEmployee));
        EmployeeRequest employeeUpdateRequest = new EmployeeRequest( "lisi", 23, "Female", 12000);
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(employeeUpdateRequest);
        mockMvc.perform(put("/employees/{id}", saveEmployee.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(204))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(saveEmployee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(saveEmployee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(employeeUpdateRequest.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(saveEmployee.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").doesNotExist());

    }



    @Test
    void should_find_employees() throws Exception {
        EmployeeRequest employee = getEmployeeRequestZhangsan();
        Employee saveEmployee = employeeJPARepository.save(EmployeeMapper.toEntity(employee));

        mockMvc.perform(get("/employees"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(saveEmployee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employee.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist());
    }

    @Test
    void should_find_employee_by_id() throws Exception {
        EmployeeRequest employee = getEmployeeRequestZhangsan();
        Employee saveEmployee = employeeJPARepository.save(EmployeeMapper.toEntity(employee));
        mockMvc.perform(get("/employees/{id}", saveEmployee.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(saveEmployee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(employee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(employee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(employee.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").doesNotExist());

    }

    @Test
    void should_delete_employee_by_id() throws Exception {
        EmployeeRequest employee = getEmployeeRequestZhangsan();
        Employee saveEmployee = employeeJPARepository.save(EmployeeMapper.toEntity(employee));

        mockMvc.perform(delete("/employees/{id}", saveEmployee.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(employeeJPARepository.findById(saveEmployee.getId()).isEmpty());
    }

    @Test
    void should_find_employee_by_gender() throws Exception {
        EmployeeRequest employee = getEmployeeRequestZhangsan();
        Employee saveEmployee = employeeJPARepository.save(EmployeeMapper.toEntity(employee));

        mockMvc.perform(get("/employees?gender={0}", "Male"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(saveEmployee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employee.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist());
    }

    @Test
    void should_find_employees_by_page() throws Exception {
        EmployeeRequest employeeZhangsan = getEmployeeRequestZhangsan();
        Employee savedZhangsan = employeeJPARepository.save(EmployeeMapper.toEntity(employeeZhangsan));
        EmployeeRequest employeeSusan = getEmployeeRequestSusan();
        Employee savedSusan = employeeJPARepository.save(EmployeeMapper.toEntity(employeeSusan));
        EmployeeRequest employeeLisi = getEmployeeRequestLisi();
        employeeJPARepository.save(EmployeeMapper.toEntity(employeeLisi));

        mockMvc.perform(get("/employees")
                        .param("page", "1")
                        .param("size", "2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedZhangsan.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employeeZhangsan.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employeeZhangsan.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employeeZhangsan.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(savedSusan.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(employeeSusan.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].age").value(employeeSusan.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].gender").value(employeeSusan.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].salary").doesNotExist());
    }
    @Test
    void should_create_employee() throws Exception {
        EmployeeRequest employee = getEmployeeRequestZhangsan();

        ObjectMapper objectMapper = new ObjectMapper();
        String employeeRequest = objectMapper.writeValueAsString(employee);
        mockMvc.perform(post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(result -> assertEquals(employeeJPARepository.count(),1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(employee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(employee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(employee.getGender()))


        ;
    }
    private static EmployeeRequest getEmployeeRequestZhangsan() {
        return new EmployeeRequest("zhangsan",22,"Male",10000);
    }
    private static EmployeeRequest getEmployeeRequestSusan() {
        return new EmployeeRequest("Susan",23,"Male",11000);
    }
    private static EmployeeRequest getEmployeeRequestLisi() {
        return new EmployeeRequest("Lisi",23,"Female",11000);
    }

}