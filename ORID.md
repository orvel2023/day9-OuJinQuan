# O
  - Code Review ：Learned why to use a few comments and the meaning of these comments, as well as the meaning of some internal fields
  - Flyway : Learned how to do version control management of SQL files. Using flyway to do version management can well record changes in sql files, backup data and improve data security.
  - Spring Boot Mapper : Learned how to map request and response data in Spring Boot, so that the request data is mapped and then added to the database. At the same time, because some information needs to be filtered when returning, the database data needs to be mapped and then returned to the front-end.
  - Cloud Native : I got to know the concept of cloud native, and the group sharing gave me a certain understanding of the technology stack contained in cloud native, including microservices, containers, and CI/CD. Most of these technologies have not been touched before, and I benefited a lot from learning today.
  - Retro:  I made a summary of the previous learning, and concluded some good points to continue to maintain and exposed some problems, which need to be corrected in the future, and got a deeper understanding of the team members.

# R
  -  Helpful!
# I
  - I think today's let me learned the new technology of Cloud Native and many java annotations. Gave me a deeper understanding of Spring Boot .
# D
  - Learn more about SQL syntax.Such DDL、DML.
  - Learn a more comprehensive java exception handling mechanism.
  - Learn new skills and go home to further study.
